// Floyd's algorithm.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <stdio.h>
#include <locale.h>
#include <malloc.h>

double min(double a,double b) {
    double min;
    if (a > b) {
        min = b;
    }
    else if(b>a) {
        min = a;
    }
    else if (a==b) {
        min = a;
    }
    return min;
}


int main()
{
    int n,m,top_name, number_of_peaks;
    double ribs_size;
    printf("number of peaks:");
    scanf_s("%d", &number_of_peaks);
    double **arr;
    arr = (double**)malloc(number_of_peaks *sizeof(double*));

    for (int i = 0; i < number_of_peaks; i++) {
        arr[i] = (double*)malloc(number_of_peaks * sizeof(double));
        for (int j = 0; j < number_of_peaks; j++) {
            if (i == j) {
                arr[i][j] = 0;
            }
            else
            {
                printf("path from vertex %d to %d:", i + 1, j + 1); scanf_s("%lf", &ribs_size);
                printf("\n");
                arr[i][j] = ribs_size;
            }
            
        }
    }
    for (int i = 0; i < number_of_peaks; i++) {
        for (int j = 0; j < number_of_peaks; j++) {
            printf("  %.2lf", arr[i][j]);
        }
        printf("\n");
    }
    for (int i = 0; i < number_of_peaks; i++) {
        for (int j = 0; j < number_of_peaks; j++) {
            for (int k = 0; k < number_of_peaks; k++) {
                arr[i][j] = min(arr[i][j], arr[i][k] + arr[k][j]);
            }
        }
    }
    for (int i = 0; i < number_of_peaks; i++) {

        for (int j = 0; j < number_of_peaks; j++) {
            if (arr[i][j] != 0) {
                printf("the shortest path from %d to %d:  %.2lf", i + 1, j + 1, arr[i][j]);
                printf("\n");
            }
        }
    }
    for (int i = 0; i < number_of_peaks; i++) {
        for (int j = 0; j < number_of_peaks; j++) {
            printf("  %.2lf", arr[i][j]);
        }
        printf("\n");
    }
}

